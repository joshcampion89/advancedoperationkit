Pod::Spec.new do |s|
  s.name = 'AdvancedOperationKit'
  s.version = '0.1'
  s.license = { :type => 'MIT', :file => 'LICENSE.txt' }
  s.summary = 'Convenience Operations to make the code from Advanced NSOperations more useful.'
  s.description = 'AdvancedOperationKit wraps the example code given by Apple in the 2015 WWDC developer presentation \'Advanced NSOperations\' into a usable framework.'
  s.homepage = 'https://bitbucket.org/joshcampion89/advancedoperationkit'
  # s.social_media_url = 'http://twitter.com/...'
  s.authors = { 'Josh Campion' => 'joshcampion89@gmail.com' }
  s.source = { :git => 'git@bitbucket.org:joshcampion89/advencedoperationkit.git', :tag => s.version }

  s.ios.deployment_target = '8.0'
  s.dependency 'PSOperations'

  s.source_files = s.source_files  = "**/AlertOperation.swift", "**/BackgroundObserver.swift", "**/NetworkObserver.swift", "**/CameraCondition.swift", "**/Convenience Operations/*.swift"
  s.exclude_files = "AdvancedOperationKitTests"
  s.requires_arc = true
end