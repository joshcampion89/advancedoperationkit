//
//  CameraCondition.swift
//  Pods
//
//  Created by Josh Campion on 20/09/2015.
//
//

import Foundation
import PSOperations

#if os(iOS)
    
    import AVKit
    import AVFoundation
    
    /// A condition for verifying access to the user's Photos library.
    struct CameraCondition: OperationCondition {
        
        static let name = "Camera"
        static let isMutuallyExclusive = false
        
        init() { }
        
        func dependencyForOperation(operation: Operation) -> NSOperation? {
            return CameraPermissionOperation()
        }
        
        func evaluateForOperation(operation: Operation, completion: OperationConditionResult -> Void) {

            switch AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) {
            case .Authorized:
                completion(.Satisfied)
                
            default:
                let error = NSError(code: .ConditionFailed, userInfo: [
                    OperationConditionKey: self.dynamicType.name
                    ])
                
                completion(.Failed(error))
            }
        }
    }
    
    /**
    A private `Operation` that will request access to the user's Camera, if it
    has not already been granted.
    */
    private class CameraPermissionOperation: Operation {
        override init() {
            super.init()
            
            addCondition(AlertPresentation())
        }
        
        override func execute() {
            switch AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo) {
            case .NotDetermined:
                dispatch_async(dispatch_get_main_queue()) {
                    AVCaptureDevice.requestAccessForMediaType(AVMediaTypeVideo, completionHandler: { (granted) -> Void in
                        self.finish()
                    })
                }
            default:
                finish()
            }
        }
        
    }
    
#endif


