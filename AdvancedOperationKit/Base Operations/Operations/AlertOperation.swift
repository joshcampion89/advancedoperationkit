/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows how to present an alert as part of an operation.
*/

import UIKit
import PSOperations

public class AlertOperation: Operation {
    // MARK: Properties

    private let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .Alert)
    private let presentationContext: UIViewController?
    
    public var title: String? {
        get {
            return alertController.title
        }

        set {
            alertController.title = newValue
            name = newValue
        }
    }
    
    public var message: String? {
        get {
            return alertController.message
        }
        
        set {
            alertController.message = newValue
        }
    }
    
    // MARK: Initialization
    
    public init(presentationContext: UIViewController? = nil) {
        self.presentationContext = presentationContext ?? UIApplication.sharedApplication().keyWindow?.rootViewController

        super.init()
        
        addCondition(AlertPresentation())
        
        /*
            This operation modifies the view controller hierarchy.
            Doing this while other such operations are executing can lead to
            inconsistencies in UIKit. So, let's make them mutally exclusive.
        */
        addCondition(MutuallyExclusive<UIViewController>())
    }
    
    public func addAction(title: String, style: UIAlertActionStyle = .Default, handler: AlertOperation -> Void = { _ in }) {
        let action = UIAlertAction(title: title, style: style) { [weak self] _ in
            if let strongSelf = self {
                handler(strongSelf)
            }

            self?.finish()
        }
        
        alertController.addAction(action)
    }
    
    override public func execute() {
        guard let presentationContext = presentationContext else {
            finish()

            return
        }

        dispatch_async(dispatch_get_main_queue()) {
            if self.alertController.actions.isEmpty {
                self.addAction("OK")
            }
            
            presentationContext.presentViewController(self.alertController, animated: true, completion: nil)
            
            if self.alertController.presentingViewController == nil {
                // there has been an error in presentation, such as this is attempting to present on a tab that isn't visible yet. So finish the operation to prevent other mutually exclusive operations from occuring.
                let error = NSError(code: .ExecutionFailed)
                self.finish([error])
            }
        }
    }
}

/// Creates an `AlertOperation` for an array of errors, appending the `localizedDescription`s of the unique errors to the `message` parameter.
public class AlertErrorOperation: AlertOperation {
    
    public init(presentationContext:UIViewController? = nil, errors:[NSError], title:String?, message:String?) {

        super.init(presentationContext: presentationContext)
        
        self.title = title
        
        var encounteredErrors = [String]()
        let uniqueErrors = errors.flatMap({ (error:NSError) -> NSError? in
            
            let errorID = error.domain + "\(error.code)" + error.localizedDescription
            
            guard !encounteredErrors.contains(errorID) else {
                return nil
            }
            
            encounteredErrors.append(errorID)
            
            return error
        })
        
        let errorDescription = uniqueErrors.reduce("") { (message:String, error:NSError) -> String in
            
            var updated = message
            if !updated.isEmpty {
                updated += "\n"
            }
            
            updated += error.localizedDescription
            
            if let failure = error.localizedFailureReason {
                updated += " \(failure)"
            }
            
            if let recovery = error.localizedRecoverySuggestion {
                updated += " \(recovery)"
            }
            
            return updated
        }
        
        var alertMessage = ""
        if let m = message {
            alertMessage += m + " "
        }
        
        self.message = alertMessage + errorDescription
    }
}
