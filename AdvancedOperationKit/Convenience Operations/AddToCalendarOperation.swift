//
//  AddToCalendarOperation.swift
//  Pods
//
//  Created by Josh Campion on 15/10/2015.
//
//

import Foundation
import EventKit
import EventKitUI

import PSOperations

public class AddToCalendarOperation: Operation, EKEventEditViewDelegate {
    
    public var completion:((event:EKEvent?, action:EKEventEditViewAction) -> ())?
    
    public var calendarEvent:EKEvent
    
    public var sourceViewController:UIViewController
    public var sourceItem:UIPopoverSourceType
    public var allowsCalendarPreview: Bool
    public var allowsEditing: Bool
    
    public static let SharedEventStore = EKEventStore()
    
    public init(calendarEvent: EKEvent, sourceViewController:UIViewController, sourceItem:UIPopoverSourceType, allowsCalendarPreview:Bool = true, allowsEditing:Bool = true) {
        
        self.calendarEvent = calendarEvent
        
        self.sourceViewController = sourceViewController
        self.sourceItem = sourceItem
        self.allowsCalendarPreview = allowsCalendarPreview
        self.allowsEditing = allowsEditing
        
        super.init()
        
        self.addCondition(Capability(EKEntityType.Event))
    }
    
    public func eventViewControllerForEvent(calendarEvent:EKEvent) -> EKEventEditViewController {
        
        let calendarVC = EKEventEditViewController()
        calendarVC.event = calendarEvent
        calendarVC.editViewDelegate = self
        calendarVC.eventStore = AddToCalendarOperation.SharedEventStore
        
        return calendarVC
    }
    
    override public func execute() {
        
        let calendarVC = eventViewControllerForEvent(calendarEvent)
        
        // configure for pop up
        switch self.sourceItem {
        case .View(let view):
            calendarVC.popoverPresentationController?.sourceView = view
            calendarVC.popoverPresentationController?.sourceRect = view.bounds
        case .BarButton(let barButton):
            calendarVC.popoverPresentationController?.barButtonItem = barButton
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.sourceViewController.presentViewController(calendarVC, animated: true, completion: nil)
        }
    }
    
    public func eventEditViewController(controller: EKEventEditViewController, didCompleteWithAction action: EKEventEditViewAction) {
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            
            self.completion?(event: controller.event, action: action)
            
            self.sourceViewController.dismissViewControllerAnimated(true, completion: nil)
            self.finish()
        }
    }
    
    override public func finished(errors: [NSError]) {
        for error in errors {
            if error.domain == OperationErrorDomain && error.code == OperationErrorCode.ConditionFailed,
                let conditionKey = error.userInfo[OperationConditionKey] as? String where conditionKey == Capability<EKEntityType>.name {
                    
                    // failed as wasn't authorized
                    let alertOperation = AlertOperation(presentationContext: sourceViewController)
                    alertOperation.title = "Permission Denied"
                    let privacyReason = (NSBundle.mainBundle().infoDictionary?["NSCalendarsUsageDescription"] as? String) ?? ""
                    alertOperation.message = "This app does not have access to your device's calendars. You can enable access in Privacy Settings.\n" + privacyReason
                    
                    alertOperation.addAction("Settings", style: .Default, handler: { (alertOperation) -> Void in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.sharedApplication().openURL(appSettings)
                        }
                    })
                    
                    alertOperation.addAction("Cancel")
                    
                    self.produceOperation(alertOperation)
            }
        }
    }
}
