//
//  ImageSelectionOperation.swift
//  Pods
//
//  Created by Josh Campion on 17/09/2015.
//
//

import Foundation
import PSOperations

import Photos
import AVKit
import MobileCoreServices

public enum UIPopoverSourceType {
    case BarButton(UIBarButtonItem)
    case View(UIView)
}

public enum UIImagePickerResultType {
    case None
    case Image(UIImage)
    case Movie(NSURL)
}

public let UIImagePickerTypeImage = kUTTypeImage as String
public let UIImagePickerTypeMovie = kUTTypeMovie as String

public class ImageSelectionOperation: BaseImageSelectionOperation {
    
    public func alertController() -> UIAlertController {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        
        // add the actions
        alert.addAction(UIAlertAction(title: "Photo Library", style: .Default, handler: { (action) -> Void in
            
            let photosOperation = PhotosSelectionOperation(sourceViewController: self.sourceViewController,
                sourceItem: self.sourceItem,
                mediaTypes:  self.mediaTypes,
                allowsEditing: self.allowsEditing,
                tint: self.tint)
            photosOperation.completion = self.completion
            
            // create a permissionOperation rather than making it a dependency to show the default UI to the user.
            self.produceOperation(photosOperation)
            
            self.finish()
        }))
        
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            
            if UIImagePickerController.isSourceTypeAvailable(.Camera) {
                
                let takePhoto = mediaTypes.contains(UIImagePickerTypeImage)
                let takeMovie = mediaTypes.contains(UIImagePickerTypeMovie)
                
                let title:String
                switch (takePhoto, takeMovie) {
                case (true, true):
                    title = "Take Photo or Video"
                case (false, true):
                    title = "Take Video"
                default:
                    title = "Take Photo"
                }
                
                alert.addAction(UIAlertAction(title: title,
                    style: .Default,
                    handler: { (action) -> Void in
                        
                        let cameraOperation = CameraOperation(sourceViewController: self.sourceViewController,
                            sourceItem: self.sourceItem,
                            mediaTypes:  self.mediaTypes,
                            allowsEditing: self.allowsEditing,
                            tint: self.tint)
                        cameraOperation.completion = self.completion
                        
                        self.produceOperation(cameraOperation)
                        
                        
                        self.finish()
                }))
            }
        }
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: { (_) -> Void in
            self.completion?(selectedMedia: .None, withInfo: nil)
            self.finish()
        }))
        
        // configure for pop up
        switch self.sourceItem {
        case .View(let view):
            alert.popoverPresentationController?.sourceView = view
            alert.popoverPresentationController?.sourceRect = view.bounds
        case .BarButton(let barButton):
            alert.popoverPresentationController?.barButtonItem = barButton
        }
        
        return alert
    }
    
    public override func execute() {
        
        let alert = alertController()
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.sourceViewController.presentViewController(alert, animated: true, completion: nil)
        }
    }
}

public class BaseImageSelectionOperation: Operation, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    public var sourceViewController:UIViewController
    public var sourceItem:UIPopoverSourceType
    public var mediaTypes:[String]
    public var tint:UIColor?
    public var allowsEditing:Bool
    
    public var completion:((selectedMedia:UIImagePickerResultType, withInfo:[String: AnyObject]?) -> ())?
    
    public init(sourceViewController:UIViewController, sourceItem:UIPopoverSourceType, mediaTypes:[String] = [kUTTypeImage as String], allowsEditing:Bool = false, tint:UIColor? = nil) {
        
        self.sourceViewController = sourceViewController
        self.sourceItem = sourceItem
        self.mediaTypes = mediaTypes
        self.allowsEditing = allowsEditing
        self.tint = tint
        
        super.init()
    }
    
    public func imagePickerControllerOfSourceType(sourceType: UIImagePickerControllerSourceType) -> UIImagePickerController {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = sourceType
        imagePicker.modalInPopover = true
        imagePicker.mediaTypes = mediaTypes
        imagePicker.allowsEditing = self.allowsEditing
        
        return imagePicker
    }
    
    // MARK: - Image Picker Delegate Methods
    
    public func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        let type = info[UIImagePickerControllerMediaType] as? String
        
        if type == UIImagePickerTypeImage {
            
            if let selectedImage = (info[UIImagePickerControllerEditedImage] as? UIImage) ?? (info[UIImagePickerControllerOriginalImage] as? UIImage) {
                completion?(selectedMedia: .Image(selectedImage), withInfo: info)
            } else {
                completion?(selectedMedia: .None, withInfo: info)
            }
            
        } else if type == UIImagePickerTypeMovie {
            
            if let movieURL = info[UIImagePickerControllerMediaURL] as? NSURL {
                completion?(selectedMedia: .Movie(movieURL), withInfo: info)
            } else {
                completion?(selectedMedia: .None, withInfo: info)
            }
            
        } else {
            completion?(selectedMedia: .None, withInfo: info)
        }
        
        self.sourceViewController.dismissViewControllerAnimated(true, completion: nil)
        
        finish()
    }
    
    public func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        
        completion?(selectedMedia: .None, withInfo: nil)
        
        self.sourceViewController.dismissViewControllerAnimated(true, completion:nil)
        
        finish()
    }
}

public class PhotosSelectionOperation: BaseImageSelectionOperation {
    
    override init(sourceViewController: UIViewController, sourceItem: UIPopoverSourceType, mediaTypes: [String], allowsEditing allowingEditing: Bool, tint: UIColor?) {
        super.init(sourceViewController: sourceViewController, sourceItem: sourceItem, mediaTypes: mediaTypes, allowsEditing: allowingEditing, tint: tint)
        self.addCondition(Capability(Photos()))
    }
    
    public override func execute() {
        
        let imagePicker = imagePickerControllerOfSourceType(.PhotoLibrary)
        imagePicker.modalPresentationStyle = .Popover
        
        switch self.sourceItem {
        case .View(let view):
            imagePicker.popoverPresentationController?.sourceView = view
            imagePicker.popoverPresentationController?.sourceRect = view.bounds
        case .BarButton(let barButton):
            imagePicker.popoverPresentationController?.barButtonItem = barButton
        }
        
        if let tintColour = self.tint {
            imagePicker.navigationBar.tintColor = tintColour
        }
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.sourceViewController.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    public override func finished(errors: [NSError]) {
        for error in errors {
            if error.domain == OperationErrorDomain && error.code == OperationErrorCode.ConditionFailed,
                let conditionKey = error.userInfo[OperationConditionKey] as? String where conditionKey ==  Capability<Photos>.name {
                    // failed as wasn't authorized
                    
                    let alertOperation = AlertOperation(presentationContext: sourceViewController)
                    alertOperation.title = "Permission Denied"
                    let privacyReason = (NSBundle.mainBundle().infoDictionary?["NSPhotoLibraryUsageDescription"] as? String) ?? ""
                    alertOperation.message = "This app does not have access to your photos or videos. You can enable access in Privacy Settings.\n" + privacyReason
                    
                    alertOperation.addAction("OK")
                    
                    alertOperation.addAction("Settings", style: .Default, handler: { (alertOperation) -> Void in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.sharedApplication().openURL(appSettings)
                        }
                    })
                    
                    self.produceOperation(alertOperation)
            }
        }
    }
}

public class CameraOperation: BaseImageSelectionOperation {
    
    override init(sourceViewController: UIViewController, sourceItem: UIPopoverSourceType, mediaTypes: [String], allowsEditing: Bool, tint: UIColor?) {
        
        super.init(sourceViewController: sourceViewController, sourceItem: sourceItem, mediaTypes: mediaTypes, allowsEditing: allowsEditing, tint: tint)
        self.addCondition(CameraCondition())
    }
    
    public override func execute() {
        
        let imagePicker = imagePickerControllerOfSourceType(.Camera)
        
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.sourceViewController.presentViewController(imagePicker, animated: true, completion: nil)
        }
    }
    
    public override func finished(errors: [NSError]) {
        for error in errors {
            if error.domain == OperationErrorDomain && error.code == OperationErrorCode.ConditionFailed,
                let conditionKey = error.userInfo[OperationConditionKey] as? String where conditionKey == CameraCondition.name {
                    
                    // failed as wasn't authorized
                    let alertOperation = AlertOperation(presentationContext: sourceViewController)
                    alertOperation.title = "Permission Denied"
                    let privacyReason = (NSBundle.mainBundle().infoDictionary?["NSCameraUsageDescription"] as? String) ?? ""
                    alertOperation.message = "This app does not have access to your device's camera. You can enable access in Privacy Settings.\n" + privacyReason
                    
                    alertOperation.addAction("OK")
                    
                    alertOperation.addAction("Settings", style: .Default, handler: { (alertOperation) -> Void in
                        if let appSettings = NSURL(string: UIApplicationOpenSettingsURLString) {
                            UIApplication.sharedApplication().openURL(appSettings)
                        }
                    })
                    
                    self.produceOperation(alertOperation)
            }
        }
    }
}
