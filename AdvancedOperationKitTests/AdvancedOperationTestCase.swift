//
//  AdvancedOperationTestCase.swift
//  Pods
//
//  Created by Josh Campion
//
//

import XCTest
@testable import AdvancedOperationKit

typealias OperationCompletionClosure = (operation: NSOperation, errors:[NSError]) -> ()

class AdvancedOperationTestCase: XCTestCase, OperationQueueDelegate {
    
    var operationExpectations = [String:XCTestExpectation]()
    
    var operationCompletions = [String:OperationCompletionClosure]()
    
    var anyOperationCompletion: OperationCompletionClosure?
    
    override func setUp() {
        super.setUp()
        
        operationExpectations.removeAll()
        operationCompletions.removeAll()
        anyOperationCompletion = nil
    }
    
    lazy var operationQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.delegate = self
        
        return queue
        }()
    
    func registerAndRunOperation(operation:NSOperation, named:String, timeout: NSTimeInterval = 10.0, handler: XCWaitCompletionHandler? = nil, completion:OperationCompletionClosure) {
        let expectation = expectationWithDescription(named)
        operation.name = named
        
        registerOperation(operation, withExpectation: expectation, completion: completion)
        
        operationQueue.addOperation(operation)
        
        waitForExpectationsWithTimeout(timeout, handler: handler)
    }
    
    func registerOperation(operation:NSOperation, withExpectation expectation:XCTestExpectation, completion:OperationCompletionClosure) {
        
        if let key = operation.name {
            operationExpectations[key] = expectation
            operationCompletions[key] = completion
        } else {
            assertionFailure("Cannot register an operation with no name.")
        }
    }
    
    func operationQueue(operationQueue: OperationQueue, operationDidFinish operation: NSOperation, withErrors errors: [NSError]) {
        
        print("operation finished: \(operation)")
        
        if let key = operation.name,
            let expectation = operationExpectations[key],
            let completion = operationCompletions[key] {
                
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    expectation.fulfill()
                    completion(operation: operation, errors: errors)
                })
                
                operationCompletions[key] = nil
                operationExpectations[key] = nil
        } else if let completion = anyOperationCompletion {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                completion(operation: operation, errors: errors)
            })
        }
    }
    
}


